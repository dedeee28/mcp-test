package main

import (
	"fmt"
	"strings"
)

type Result struct {
	Number1 []int
	Number2 []int
	Number3 []string
}

func test1(nums []int) (result []int) {
	if len(nums) == 0 {
		return
	}

	n := nums[0]

	for _, value := range nums {
		if n < value {
			n = value
		}
	}

	return append(result, n)
}

func test2(nums []int, x int) (result []int) {
	listMap := make(map[int]int)
	xMap := make(map[int]int)

	for _, value1 := range nums {
		for _, value2 := range nums {
			if (value2 / value1) == x {
				if _, exists := xMap[value2]; !exists {
					xMap[value2] = value2
					continue
				}
			}

			if _, exists := listMap[value2]; !exists {
				if listMap[value2] != xMap[value2] {
					listMap[value2] = value2
					result = append(result, value2)
				}
			}
		}
	}
	return
}

func test3(word string, x int) (result []string) {
	str := strings.Split(word, " ")
	for _, value := range str {
		if len(value) == x {
			result = append(result, value)
		}
	}
	return
}

func main() {
	var result Result

	//1
	nums := []int{3, 1, 4, 2}
	result.Number1 = test1(nums)

	//2
	nums = []int{5, 20, 4, 10, 100, 50}
	x := 5
	result.Number2 = test2(nums, x)

	//3
	word := "souvenir loud four lost "
	x = 4
	result.Number3 = test3(word, x)

	fmt.Printf("1. Result: %v \n", result.Number1)
	fmt.Printf("2. Result: %v \n", result.Number2)
	fmt.Printf("3. Result: %v \n", result.Number3)
}
